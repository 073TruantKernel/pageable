# Pageable

This library provides only a simple interface for pagination.

To implement this interface three methods must be provided: `getSize()` `getOffset()` and `getLimit()`